/*
 * @Descripttion: 右下角改变大小
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-20 11:16:16
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-05 15:43:19
 */
import { getMousePos, iframeAreaStr2int } from '../../utils/tool'
import utils from '../../utils/utils'

export default {
  beforeDestroy () {
    utils.unbind(document, 'mousemove', this.handleMouseMove)
    utils.unbind(document, 'mouseup', this.handleMouseUp)
  },
  methods: {
    onResizeMouseDown (e) {
      this.mousePosOld = getMousePos(e)
      utils.bind(document, 'mousemove', this.handleMouseMove)
      utils.bind(document, 'mouseup', this.handleMouseUp)
    },
    handleMouseUp (e) {
      this.mousePosOld = null
      utils.unbind(document, 'mousemove', this.handleMouseMove)
    },
    handleMouseMove (e) {
      setTimeout(this.handleMove, 100, e)
    },
    handleMove (e) {
      if (this.mousePosOld !== null) {
        let mousePos = getMousePos(e)
        let mouseDisPos = [
          mousePos[0] - this.mousePosOld[0],
          mousePos[1] - this.mousePosOld[1]
        ]
        let intArea = iframeAreaStr2int(this.currentArea)
        let targetAreaInt = [
          (intArea.width + mouseDisPos[0]),
          (intArea.height + mouseDisPos[1])
        ]
        if (!this.setCurrentArea(targetAreaInt)) return
        this.mousePosOld = getMousePos(e)
      }
    },
    setCurrentArea (targetAreaInt) {
      const minW = 200
      const minH = 100 - this.titleHeight
      if (targetAreaInt[0] <= minW) return false
      if (targetAreaInt[1] <= minH) return false
      const targetArea = [`${targetAreaInt[0].toString()}px`, `${targetAreaInt[1].toString()}px`]
      this.currentArea = targetArea
      this.normalArea = targetArea
      return true
    }
  }
}
