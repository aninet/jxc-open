
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:33:34
 */
import { getBillAndDetail } from '@/api/bill/bill-purchase.js'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        this.dataObj = bill
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initViewData() {
      const billId = this.billId
      this.$refs.GoodsSelect.setTableLoading(true)
      return this.getBillContainGoodsData(billId).then(data => {
        this.$refs.GoodsSelect.setTableLoading(false)
        const { bill, billDetailList } = data
        this.dataObj = bill
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initTransData() {
      const billId = this.billId
      this.setTableLoading(true)
      return this.getBillContainGoodsData(billId).then(data => {
        this.setTableLoading(false)
        const { bill, billDetailList } = data
        bill.billRelationNo = bill.billNo
        bill.billRelationId = bill.id
        this.$delete(bill, 'id')
        this.$delete(bill, 'billNo')
        billDetailList.forEach(item => {
          const { quantity, changeQuantity } = item
          const canTransQty = this.$amount(quantity).subtract(changeQuantity).format()
          this.$set(item, 'quantity', canTransQty)
        })
        this.setDataObj(bill)
        this.$refs.GoodsSelect.setTableData(billDetailList)
        this.getBillCode()
      })
    },
    setDataObj(dataObj) {
      Object.keys(dataObj).forEach(key => {
        this.$set(this.dataObj, key, dataObj[key])
      })
    },
    getBillContainGoodsData(billId) {
      return new Promise((resolve, reject) => {
        getBillAndDetail(billId).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

