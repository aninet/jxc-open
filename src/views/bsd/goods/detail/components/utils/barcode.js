/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-04 12:37:50
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-20 13:09:20
 */

export const initDefaultBarcodeData = function(unitData) {
  return initBarcodeData(unitData, [])
}

export const initUpdateBarcodeData = function(unitData, goodsBarcodeList) {
  return initBarcodeData(unitData, goodsBarcodeList)
}

/**
 * 初始化条形码数据
 * @param {c} unitData
 * @param {*} goodsBarcodeList
 */
function initBarcodeData(unitData, goodsBarcodeList) {
  const result = []
  unitData.forEach((item, index) => {
    if (item.value) {
      const blankObj = { unitId: item.value, unitName: item.label, barcode: '' }
      const goodsBarcodeObj = goodsBarcodeList[index]
      if (goodsBarcodeObj) {
        if (!goodsBarcodeObj.barcode) goodsBarcodeObj.barcode = ''
      }
      const obj = goodsBarcodeObj || blankObj
      result.push(obj)
    }
  })
  return result
}
