/*
 * @Descripttion:
 * @version:
 * 1. 商品表，列可以修改替换
 * 2. simleSelect 列props
 * @Author: cxguo
 * @Date: 2019-10-18 09:21:53
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-24 13:29:57
 */

import Main from './main'
import VXETable from 'vxe-table'
import { installRender } from './utils'
installRender(VXETable)
export default Main
